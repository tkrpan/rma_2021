package com.example.recyclerviewapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NameClickListener{

    private static final String TAG = "MainActivity";

    private RecyclerView recyclerView;
    private List<String> dataList;
    private CustomAdapter customAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupData();
        setupRecyclerView();
    }

    private void setupData(){
        dataList = new ArrayList<>();
        dataList.add("Anna");
        dataList.add("Milovan");
        dataList.add("Marko");
        dataList.add("Janko");
        dataList.add("Petar");
        dataList.add("Ivona");
        dataList.add("Marija");
        dataList.add("Jakov");
    }

    private void setupRecyclerView(){
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        customAdapter = new CustomAdapter(dataList, this);
        recyclerView.setAdapter(customAdapter);
    }

    @Override
    public void onNameClick(int position) {
        Log.d(TAG, " onNameClick position " + position);
    }
}